package PatienceSort;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class PS {
    public static <T extends Comparable<? super T>> void sort(T[] n) {
        List<Pile<T>> piles = new ArrayList<>();
        for (T x : n) {
            Pile<T> newPile = new Pile<>();
            newPile.push(x);
            int i = Collections.binarySearch(piles, newPile);
            if (i < 0) {
                i = ~i;
            }
            if (i != piles.size()) {
                piles.get(i).push(x);
            } else {
                piles.add(newPile);
            }
        }


        PriorityQueue<Pile<T>> heap = new PriorityQueue<>(piles);
        for (int i = 0; i < n.length; i++) {
            Pile<T> smallPile = heap.poll();
            n[i] = smallPile.pop();
            if (!smallPile.isEmpty()) {
                heap.offer(smallPile);
            }
        }
    }

    private static class Pile<E extends Comparable<? super E>> extends Stack<E> implements Comparable<Pile<E>> {
        public int compareTo(Pile<E> y) {
            return peek().compareTo(y.peek());
        }
    }

    public long trigger(File f) throws FileNotFoundException {
        List<Integer> list = new ArrayList<>();
        Scanner in = new Scanner(f);
        while (in.hasNextInt()) {
            list.add(in.nextInt());
        }
        Integer[] ar = new Integer[list.size()];
        list.toArray(ar);
        long before = System.currentTimeMillis();
        sort(ar);
        long after = System.currentTimeMillis();
        System.out.println(Arrays.toString(ar));
        return after - before;
    }
}